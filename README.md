# LEAK MONITOR

| **Current version** | **Changelog** | **Tested on**      |
| --- | --- | --- |
| `v0` | First release | Debian GNU/Linux 10 | 

## About
*Leak monitor* is a tool that allows the user to find new leaks from some given sources, to keep track of the relevant ones and eventually to check if a password has been compromised.
*Leak monitor* automatically runs the scrapers associated to some predefined leak sources, so that the user can easily add features to the specific scrapers or even add a new leak source and its scraper, just by inserting a row in the database.
Furthermore, *Leak monitor* allows to store companies names and notifies the user any time there's a leak related to those companies.

* No computer skills needed
* Look for **new** leaks only
* Automatically download leaks, check for passwords and store them
* Parallel downloading
* Verify if your passwords have been compromised
* Verify if your company has been compromised
* Build your own scraper
* Simply add a new leak source to check
* Always keep an eye on the stored data


## Getting started
```bash
git clone https://gitlab.com/andrea.314/leak-monitor.git
cd leak_monitor
pip3 install -r requirements.txt
chmod +x leak_monitor.py
python3 leak_monitor.py [options]
```
> Python >= 3.6, a local MongoDB Server and a Chrome web driver are also required.
Make sure that the chromedriver is in your PATH enviroment variable and that the mongod service is active.

## Usage
```bash
Usage: python3 leak_monitor.py [options]
Options:
	-h, --help						show this help message and exit
	-i, --init						initialize local MongoDB database  	
	-d, --download						download new leaks (if present)                        
	-p [{collection-name} [{collection-name} ...]],
	--printdb [{collection-name} [{collection-name} ...]]
								print specified db collections or the whole db

```


