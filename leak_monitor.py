import pymongo
import datetime
import subprocess
import argparse
import print_db
import init_db

## local db
myclient = pymongo.MongoClient("mongodb://localhost:27017/")

mydb = myclient["leaks_db"]

# collections
leaks=mydb["leaked_passwords"]
md=mydb["metadata"]
given=mydb["given_hashes"]
sources=mydb["leak_sources"]

## check matching hashes
def check_passwords():
	for x in given.find({}, { "_id": 0, "hash": 1 }):
		xhash=x["hash"].strip()
		search=leaks.find({ "hash": xhash }, { "source": 1 })
		for y in search:
			print(f"FOUND HASH {xhash} IN \"{y['source']}\"")
			print(md.find_one({ "source": y["source"] }, { "_id": 0 }))
	print()


## analyze leak sources
def check_sources():
	for source in sources.find({}, { "_id": 1, "name": 1, "scraper": 1 }):
		print(f"\nAnalyzing {source['name']} ...")

		if source["scraper"]:
			## execute the scraper associated to the current leak source
			call=["python3", source['scraper']]
			if args.download:
				call.append("-d")
			#f=open(source['scraper'])
			#exec(f.read())
			#f.close()
			p=subprocess.Popen(call)
			p.wait() # consider using threading or multiprocessing to execute all scrapers

			## update last_check field
			newvalue={ "$set": { "last_check": datetime.datetime.now().strftime("%x %X") } }
			#newvalue={ "$set": { "last_check": datetime.datetime(2010, 1, 1).strftime("%x %X") } } # remove (only for testing)
			sources.update_one({ "_id": source["_id"] }, newvalue)

			if args.download:
				check_passwords()

if __name__ == '__main__':

	## handle options
	parser=argparse.ArgumentParser(description="Find new leaks")
	parser.add_argument("--download", "-d", action='store_true')
	parser.add_argument("--printdb", "-p", nargs='*', choices=mydb.list_collection_names())
	parser.add_argument("--init", "-i", action='store_true')
	args=parser.parse_args()

	## init db
	if args.init:
		init_db.init_db()

	## print db
	elif args.printdb != None: # True if args.printb is empty, False if args.printdb=None
		if len(args.printdb) == 0:
			args.printdb=mydb.list_collection_names()
		print_db.print_db(args.printdb)

	## no args or --download
	else:
		check_sources()
