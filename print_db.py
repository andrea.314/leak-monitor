import pymongo
import argparse

## local db
myclient = pymongo.MongoClient("mongodb://localhost:27017/")

mydb = myclient["leaks_db"]

# collections
leaks=mydb["leaked_passwords"]
md=mydb["metadata"]
given=mydb["given_hashes"]
sources=mydb["leak_sources"]

## print collections
def print_db(collections):
	for x in collections:
		print(f"COLLECTION {x}:")
		try:
			for y in mydb[x].find({}, { "_id": 0 }):
				print(y)
		except:
			print(f"Collections {collections} have not been created/populated")
			continue
		print()
	print()


if __name__ == '__main__':

	dblist=myclient.list_database_names()
	if "leaks_db" not in dblist:
		raise Exception("Database \"leaks_db\" has not been created/populated")

	## handle options
	parser=argparse.ArgumentParser()
	parser.add_argument("--collections", "-c", nargs='*', default=mydb.list_collection_names(), choices=mydb.list_collection_names())
	args=parser.parse_args()

	print_db(args.collections) # if there are no args args.collections will contain all the collections
