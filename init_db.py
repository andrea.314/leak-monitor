import pymongo
import hashlib
import datetime

## local db
myclient = pymongo.MongoClient("mongodb://localhost:27017/")

mydb = myclient["leaks_db"]

# collections
leaks=mydb["leaked_passwords"]
md=mydb["metadata"]
given=mydb["given_hashes"]
sources=mydb["leak_sources"]
comp=mydb["companies"]


def init_db():
	leaks.drop()
	md.drop()
	given.drop()
	sources.drop()
	comp.drop()

	## create an ascending single field index on the "hash" field of the "leaks" collection
	leaks.create_index([("hash", pymongo.ASCENDING)])

	## check if db has been created
	# in mongodb a database is not created until it gets content (collections and documents)
	dblist = myclient.list_database_names()
	if "leaks_db" not in dblist:
		raise Exception("Database \"leaks_db\" has not been created/populated")

	## check if collection has been created
	collist=mydb.list_collection_names()
	if "leaked_passwords" not in collist:
		raise Exception("Collection \"leaked_passwords\" has not been created/populated")


	#manually populate the collection "given" (only for testing)
	hash=hashlib.pbkdf2_hmac('sha256', 'Clubbanger1'.encode('utf-8'), b'', 1).hex().strip()
	wrong1=hashlib.pbkdf2_hmac('sha256', 'Wrong1'.encode('utf-8'), b'', 1).hex().strip()
	wrong2=hashlib.pbkdf2_hmac('sha256', 'Wrong2'.encode('utf-8'), b'', 1).hex().strip()
	doc=[
		{ "hash": hash },
		{ "hash": wrong1 },
		{ "hash": wrong2 }
	]
	given.insert_many(doc)

	collist=mydb.list_collection_names()
	if "given_hashes" not in collist:
		raise Exception("Error in creating the collection \"given_hashes\"")


	## manually populate collection leak_sources
	doc=[
		{
			"name": "RANSOMEXX",
			"url": "http://rnsm777cdsjrsdlbs4v5qoeppu3px6sb2igmh53jzrx7ipcrbjz5b2ad.onion",
			"scraper": "ransomexx_scraper.py",
			"last_check": datetime.datetime(2010,1,1).strftime("%x %X") # dummy value to reset db
			# %x local date and %X local time (e.g. 09/25/2021 00:00:00)
		},
		{
			"name": "Twitter \#infoleak",
			"url": "https://twitter.com/hashtag/infoleak?f=live",
			"scraper": "twitter_scraper.py",
			"last_check": datetime.datetime(2010,1,1).strftime("%x %X")
		}
	]

	sources.insert_many(doc)

	collist=mydb.list_collection_names()
	if "leak_sources" not in collist:
		raise Exception("Error in creating the collection \"leak sources\"")



	## create an ascending single field index on the "name" field of the "companies" collection
	comp.create_index([("name", pymongo.ASCENDING)])

	## manually populate collection companies
	comp.insert_one({ "name": "calamp (nasdaq: camp)" })

	collist=mydb.list_collection_names()
	if "leak_sources" not in collist:
		raise Exception("Error in creating the collection \"leak sources\"")



	print("Database \"leaks_db\" correctly initialized\n(collections are not created until they get content)")
