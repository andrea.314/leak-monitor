import requests
from requests_tor import RequestsTor
from bs4 import BeautifulSoup
import sys
import os
import threading
import re
import shutil
from func_timeout import func_timeout, FunctionTimedOut, func_set_timeout
from pathlib import Path
from zipfile import ZipFile
import csv
import hashlib
import pymongo
import datetime
import argparse

## RANSOMEXX homepage
url='http://rnsm777cdsjrsdlbs4v5qoeppu3px6sb2igmh53jzrx7ipcrbjz5b2ad.onion/'

threads=[]

downloads_path=os.path.expandvars("$PWD")+os.sep+"archive"+os.sep+"RANSOMEXX"
try:
	Path(downloads_path).mkdir(parents=True, exist_ok=True)
except:
	sys.exit(f"Creation of the directory \"{downloads_path}\" failed")

extracted_path=os.path.join(downloads_path, "extracted_leaks")
try:
	Path(extracted_path).mkdir(parents=True, exist_ok=True)
except:
	sys.exit(f"Creation of the directory \"{extracted_path}\" failed")

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb=myclient["leaks_db"]
leaks=mydb["leaked_passwords"]
md=mydb["metadata"]
sources=mydb["leak_sources"]
comp=mydb["companies"]

## download single leak
def download_leak(company, up_date, visits, total_size, leak_url):

	dirpath=os.path.join(downloads_path, company.strip().replace(" ", "_"))
	try:
		Path(dirpath).mkdir(parents=True, exist_ok=True)
	except:
		sys.exit(f"Creation of the directory \"{dirpath}\" failed")

	## go to leak page
	for i in range(3):
		try:
			leak_soup=BeautifulSoup(rt.get(leak_url).content, 'html.parser')
			break
		except:
			print(f"Couldn't get {leak_url}, attempt {i+1}/3")

	# download_links contains all the parts (links) of a single leak
	download_links=leak_soup.find_all("a", href=re.compile("upload\.zip\.[0-9]{3}")) ### add arg "limit=N" to limit components
	parts_num=len(download_links) # number of leak components
	for dl in download_links:
		component_url=dl.get('href')
		file_size=dl.find_all("b")[1].get_text()

		## parallel download (threading)
		#start_downloader(download_partial, args=[company, component_url, file_size, parts_num])

		## sequential download
		download_partial(company, component_url, file_size, parts_num)

		## parallel download (multiprocessing)
		#TODO

		#break

	#break

	## wait for all threads to finish (only if using threading)
	#for t in threads:
	#	t.join()

	print(f"\tFinished downloading all leaks related to {company}\n")

	## retrieve zip password from the whole leak
	pw=download_links[0].find_next_siblings("b")[1].get_text()

	return pw


## define and start thread
def start_downloader(target_func, args=[]):
	download_thread=threading.Thread(target=target_func, name="Downloader", args=args)
	#download_thread.setDaemon(True)
	threads.append(download_thread)
	download_thread.start()

## set timeout for the download
def download_partial(company, url, file_size, parts_num):
	tor_timeout=1800 # 30 min
	try:
		func_timeout(tor_timeout, effective_download, args = [company, url, file_size, parts_num])
	except FunctionTimedOut:
		sys.exit("Tor session terminated after {} minutes timeout.".format(tor_timeout/60))

## download leak subpart
def effective_download(company, url, file_size, parts_num):
	filename=url.split(os.sep)[-1]
	filenumber=int(filename[-3:])
	filepath=downloads_path+os.sep+company.strip().replace(" ", "_")+os.sep+filename
	print(f"\tDownloading {filename} ...\n\t[ part {filenumber}/{parts_num}, size: {file_size}, company: {company} ]\n")
	try:
		leak_req=rt.get(url) # fire request

		with open(filepath, 'wb') as outfile:
			outfile.write(leak_req.content)
			outfile.close()
			#shutil.copyfileobj(leak_req.raw, oufile)
			#shutil.copyfileobj(response, oufile)
			print(f"\tSuccessfully downloaded {filename} of company: {company}\n")
	except:
		sys.exit(f"Couldn't download file located at {url}")


## look for passwords or hashes and eventually update db
def check_leak_update_db(pw, company, up_date, visits, total_size):
	wf_company=company.strip().replace(" ", "_")
	newdir=os.path.join(extracted_path, wf_company)
	try:
		Path(newdir).mkdir(parents=True, exist_ok=True)
	except:
		sys.exit(f"Creation of the directory \"{dirpath}\" failed")

	for f in os.scandir(downloads_path+os.sep+wf_company):
		if f.is_file():
			with ZipFile(f, 'r') as zip:
				zip.extractall(newdir, pwd=bytes(pw, 'utf-8'))

	print(f"\tAll files extracted in {newdir}\n")

	for f in os.scandir(newdir):
		if f.is_file():
			print(f"\tChecking {f.name} ...")
			with open(newdir+os.sep+f.name, newline='') as csvfile:
				reader=csv.reader(csvfile)
				try:
					first=reader.__next__()
					for x in first:
						if re.match("\s*(password|pw|pwd|plain(text)?)\s*", x, re.IGNORECASE):
							print("\tPotential plaintext passwords found!")
							col=first.index(x)
							for row in reader:
								plaintext=row[col].strip()
								hex_hash=hashlib.pbkdf2_hmac('sha256', plaintext.encode('utf-8'), b'', 1)
								hash=hex_hash.hex().strip()
								if not leaks.find_one({ "hash": hash }, {}):
									x=leaks.insert_one({ "hash": hash, "plaintext": plaintext, "source": "RANSOMEXX" })
							print("\tAll new passwords stored in the database\n")

						elif re.match("hash|(hash(ed)*)*\s*(password|pw|pwd)+", x, re.IGNORECASE):
							print("\tPotential hashed passwords found!")
							col=first.index(x)
							for row in reader:
								hash=row[col].strip()
								if not leaks.find_one({ "hash": hash }, {}):
									x=leaks.insert_one({ "hash": hash, "plaintext": None, "source": "RANSOMEXX" })
							print("\tAll new passwords stored in the database\n")

				except csv.Error as e:
					sys.exit('file {}, line {}: {}'.format(f.name, reader.line_num, e))

			csvfile.close()



## verify if the current company belongs to the collection companies
def check_companies(company, up_date, visits, total_size, leak_url):
	search=comp.find({ "name": company.lower() }, { "name": 1 })
	for x in search:
		print(f"FOUND LEAK related to company \"{x['name']}\"")
		if not args.download:
			res=input("Do you want to download it now (y/n)? ")
			if res.lower() == "y":
				download_leak(company, up_date, visits, total_size, leak_url)




### using library requests_tor

## handle options
parser=argparse.ArgumentParser(description="Search RANSOMEXX for new leaks and eventually download them")
parser.add_argument('--download', '-d', action='store_true')
args=parser.parse_args() # args is a namespace object (just an object subclass with a readable string representation)
#vars(args) # dict-like view of the attributes

## get onion website
print(f"Getting {url} ...")
rt=RequestsTor(tor_ports=(9050,), tor_cport=9051)
r=rt.get(url) # r is a requests.Response object

## scrape the web page with BeautifulSoup
soup=BeautifulSoup(r.content, features="lxml")
# in the ransomexx homepage each leak has a "read more" button
read_more_links=soup.find_all("a", string=re.compile("Read more"))
#read_more_links=soup.find_all("a", class_="btn btn-outline-primary")

# get last check date from local db
last_check_str=sources.find({ "name": "RANSOMEXX" }, { "_id": 0, "last_check": 1 })[0]["last_check"]
last_check=datetime.datetime.strptime(last_check_str, '%m/%d/%y %H:%M:%S')
last_check=datetime.datetime(2020,8,25,0,0,0) ### remove (only for testing)
print(f"Last check: {last_check.strftime('%x %X')}")

for link in read_more_links:
	## retrieve company name
	company=link.find_previous_sibling("h5").get_text()

	## retrieve metadata
	metastr=link.find_next_sibling("p").get_text()
	metalist=re.split(',\s|:\s', metastr)
	up_date=datetime.datetime.strptime(metalist[1], '%Y-%m-%d')
	visits=int(metalist[3])
	total_size=metalist[5]

	## check if new leak
	#print(f"updated: {up_date.strftime('%x %X')}\nlast check: {last_check.strftime('%x %X')}")
	if last_check > up_date:
		sys.exit("No more new leaks!\n")

	leak_url=url+link['href'][1:] # url of the single leak

	print(f"NEW LEAK!\n\tCompany: {company}\n\turl: {leak_url}\n")

	## store metadata
	if not md.find_one({ "leak name": company }, {}):
		x=md.insert_one({ "source": "RANSOMEXX", "leak name": company, "updated": up_date.strftime("%x"), "leak url": leak_url, "visits": visits, "size": total_size })
		print(f"\tStored metadata:")
		print("\t", md.find({ "_id": x.inserted_id }, { "_id": 0 })[0], "\n")

	## verify if the current company belongs to the collection companies
	check_companies(company, up_date, visits, total_size, leak_url)

	## (if requested) download leaks, extract them and store found passwords
	if args.download:
		print(f"Downloads path: {downloads_path}\n")
		## download single leak
		pw=download_leak(company, up_date, visits, total_size, leak_url)

		## look for passwords or hashes and eventually update db
		check_leak_update_db(pw, company, up_date, visits, total_size)





### without library requests_tor
"""
# Make a request through the Tor connection (IP visible through Tor)
def get_tor_session():
	session = requests.session()
	# Tor uses the 9050 port as the default socks port
	session.proxies = {'http':  'socks5h://127.0.0.1:9050',
	                   'https': 'socks5h://127.0.0.1:9050'}
	return session

session = get_tor_session()

# define request header
#headers = {}
#headers['User-agent'] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36"
# or:
#ua_list = ["Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.19577"
#,"Mozilla/5.0 (X11) AppleWebKit/62.41 (KHTML, like Gecko) Edge/17.10859 Safari/452.6", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2656.18 Safari/537.36"
#,"Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML like Gecko) Chrome/44.0.2403.155 Safari/537.36", "Mozilla/5.0 (Linux; U; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.2.149.27 Safari/525.13","Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27"
#,"Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10_5_8; zh-cn) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27"]
#ua = random.choice(ua_list)
#headers = {'User-Agent': ua}

# print an IP different than your public IP
#result = session.get(url)

# scrape site using BeautifulSoup
#soup=BeautifulSoup(result.content, 'html.parser')
"""
