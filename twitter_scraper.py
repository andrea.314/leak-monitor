import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
import time
import datetime
import sys
import argparse
import pymongo
import os
import glob
import re
from pathlib import Path

def check_leak_update_db(filepath):
	filename=os.path.split(filepath)[-1]
	print(f"Checking {filename} ...")
	try:
		for line in open(filepath):
			sha256=re.findall(r'\b[a-f0-9]{64}\b', line, re.I)
			for hash in sha256:
				if not leaks.find_one({ "hash": hash }, {}):
					x=leaks.insert_one({ "hash": hash, "plaintext": None, "source": "twitter \#infoleak" })

			md5=re.findall(r'\b[a-f0-9]{32}\b', line, re.I)
			for hash in md5:
				if not leaks.find_one({ "hash": hash }, {}):
					x=leaks.insert_one({ "hash": hash, "plaintext": None, "source": "twitter \#infoleak" })

		print("All new hashes stored in the database\n")
	except:
		print(f"Error while reading {filename}\n")


URL='https://twitter.com/hashtag/infoleak?f=live'
#URL='https://twitter.com/i/lists/203915919'
downloads_path=os.path.expandvars("$PWD")+os.sep+"archive"+os.sep+"twitter_infoleak"
try:
	Path(downloads_path).mkdir(parents=True, exist_ok=True)
except:
	sys.exit(f"Creation of path \"{downloads_path}\" failed")

## connect to local db
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb=myclient["leaks_db"]
leaks=mydb["leaked_passwords"]
md=mydb["metadata"]
sources=mydb["leak_sources"]
comp=mydb["companies"]

## handle options
parser=argparse.ArgumentParser(description="Search twitter \#infoleak for new pastes containing hashes")
parser.add_argument('--download', '-d', action='store_true')
args=parser.parse_args()

## get last check date from local db
last_check_str=sources.find({ "name": "RANSOMEXX" }, { "_id": 0, "last_check": 1 })[0]["last_check"]
last_check=datetime.datetime.strptime(last_check_str, '%m/%d/%y %H:%M:%S')
last_check=datetime.datetime(2021,12,5,8,0,0) ### remove (only for testing)
print(f"Last check: {last_check.strftime('%x %X')}")


### SELENIUM WITH CHROME

## configure chrome driver
print("SELENIUM - CHROME")
chrome_options=Options()
#chrome_options.headless=True
chrome_options.add_argument('--headless')
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')
preferences={ "download.default_directory": downloads_path,
		"directory_upgrade": True,
		"safebrowsing.enabled": True }
chrome_options.add_experimental_option("prefs", preferences)
driver = webdriver.Chrome(options=chrome_options) # make sure chromedriver is in the PATH variable

print(f"Getting {URL} ...")
driver.get(URL) #print(driver.page_source)
driver.maximize_window()
driver.refresh()


## wait for the web page to fully load
print("Waiting for the web page to fully load...")
try:
    # wait 5 sec for that element to be loaded
    element = WebDriverWait(driver, 5).until(
	EC.presence_of_element_located((By.CLASS_NAME, 'css-1dbjc4n'))
    )
except:
	sys.exit(f"Problems in loading the page... retry later")

driver.implicitly_wait(5)


## scrape links
print("Scraping the website...\n")

# other find_elements() tecniques
"""
twits_div=driver.find_elements(By.XPATH, "//div[@class='css-901oao r-18jsvk2 r-37j5jr r-a023e6 r-16dba41 r-rjixqe r-bcqeeo r-bnwqim r-qvutc0']")

links=driver.find_elements(By.PARTIAL_LINK_TEXT, "scrape.pastebin.com")
for link in links:
	print(link.get_attribute('text'))

links=driver.find_elements(By.XPATH, "//a[@class='css-4rbku5 css-18t94o4 css-901oao css-16my406 r-1cvl2hr r-1loqt21 r-poiln3 r-bcqeeo r-qvutc0']")
for link in links:
	print(link.get_attribute('text'))

leak_types=driver.find_elements(By.XPATH, "//a[@class='css-4rbku5 css-18t94o4 css-901oao css-16my406 r-1cvl2hr r-1loqt21 r-poiln3 r-bcqeeo r-qvutc0']/../span[@class='css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0']")
for type in leak_types:
	print(type.get_attribute('innerHTML'))
"""

# try loading more twits by scrolling the page
"""
for i in range(10):
	driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
#	driver.execute_script("window.scrollBy(0, 10000);")
	driver.implicitly_wait(5)
driver.implicitly_wait(5)
"""

for i in range(20):
	driver.implicitly_wait(10)
	dates=driver.find_elements(By.XPATH, "//a[@class='css-4rbku5 css-18t94o4 css-901oao r-14j79pv r-1loqt21 r-1q142lx r-37j5jr r-a023e6 r-16dba41 r-rjixqe r-bcqeeo r-3s2u2q r-qvutc0']/time")

twit_dates=[]
for d in dates:
	d_str=d.get_attribute('datetime')
	dt=datetime.datetime.strptime(d_str, '%Y-%m-%dT%H:%M:%S.000Z')
	if last_check >= dt:
		break
	twit_dates.append(dt)

if len(twit_dates) == 0:
	sys.exit("No new leaks found!")
else:
	print(f"{len(twit_dates)} new leaks found!\n")

links=driver.find_elements(By.XPATH, "//div[@class='css-901oao r-18jsvk2 r-37j5jr r-a023e6 r-16dba41 r-rjixqe r-bcqeeo r-bnwqim r-qvutc0']/a[@class='css-4rbku5 css-18t94o4 css-901oao css-16my406 r-1cvl2hr r-1loqt21 r-poiln3 r-bcqeeo r-qvutc0']")
#types=driver.find_elements(By.XPATH, "//div[@class='css-901oao r-18jsvk2 r-37j5jr r-a023e6 r-16dba41 r-rjixqe r-bcqeeo r-bnwqim r-qvutc0']/span[@class='css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0']")
types=driver.find_elements(By.XPATH, "//div[@class='css-901oao r-18jsvk2 r-37j5jr r-a023e6 r-16dba41 r-rjixqe r-bcqeeo r-bnwqim r-qvutc0']/a[@class='css-4rbku5 css-18t94o4 css-901oao css-16my406 r-1cvl2hr r-1loqt21 r-poiln3 r-bcqeeo r-qvutc0']/../span[@class='css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0']")

links=links[:len(twit_dates)]
types=types[:len(twit_dates)]

for i in range(len(twit_dates)):
	print(twit_dates[i].strftime('%x %X'))
	print(links[i].get_attribute('text'))
	print(types[i].get_attribute('innerHTML'))
	print()


print("Filtering only the pastes containing hashes...")
hash_links=[]
for type in types:
	descr=type.get_attribute('innerHTML')
	if "Hashes" in descr:
		type_index=types.index(type)
		link=links[type_index].get_attribute('text')
		hash_date=twit_dates[type_index]
		types[type_index]="k" # dummy value to remember that the current type has already
					# been considered without changing the list indexes
		print(f"{hash_date.strftime('%x %X')}\n{descr}\n{link}\n")
		hash_links.append({ "link": link, "date": hash_date })

if(len(hash_links)==0):
	sys.exit("No links containing hashes found :( retry later!")

## store metadata
for link in hash_links:
	paste_name=link['link'].split("/")[-1]
	if not md.find_one({ "leak name": paste_name }, {}):
		x=md.insert_one({ "source": "twitter \#infoleak", "leak name": paste_name, "updated": link['date'].strftime('%x %X'), "leak_url": link['link'], "visits": None, "size": None }) 
		print("Stored metadata:")
		print(md.find({ "_id": x.inserted_id }, { "_id": 0 })[0], "\n")

"""
## THIS SOL CANNOT WORK (the div id is dinamically generated during every page reload)
twit_divs=driver.find_elements(By.XPATH, "//div[@class='css-901oao r-18jsvk2 r-37j5jr r-a023e6 r-16dba41 r-rjixqe r-bcqeeo r-bnwqim r-qvutc0']")
for twit_div in twit_divs:
	div_id=twit_div.get_attribute('id')
	print(div_id)
	#leak_descr=driver.find_element(By.XPATH, "//div[@id=div_id]/span[@class='css-901oao css-16my406 r-poiln3 r-bcqeeo r-qvutc0']").get_attribute('innerHTML')
	#print(leak_descr)
	#link=driver.find_element(By.XPATH, "//div[@id=div_id]/a[@class='css-4rbku5 css-18t94o4 css-901oao css-16my406 r-1cvl2hr r-1loqt21 r-poiln3 r-bcqeeo r-qvutc0']")
	#print(link.get_attribute('text'))
"""

if args.download:
	print(f"Downloads path: {downloads_path}\n")

	## visit link and download content
	### first link is out of the loop to handle cookies and login request ###

	url=hash_links[0]['link'].replace("scrape", "www")
	print(f'Getting {url} ...')
	driver.get(url)

	# accept privacy policy
	try:
		element = WebDriverWait(driver, 10).until(
			EC.presence_of_element_located((By.CLASS_NAME, 'qc-cmp2-summary-buttons'))
		)
	except TimeoutException:
		pass # no privacy policy
	else:
		driver.find_element(By.XPATH, "//button[@class='sc-ifAKCX ljEJIv']").click()

	# accept cookies
	try:
		element = WebDriverWait(driver, 5).until(
			EC.presence_of_element_located((By.CLASS_NAME, 'popup-container'))
			#EC.presence_of_element_located((By.CLASS_NAME, 'cookie-button js-close-cookies'))
		)
	except TimeoutException:
		pass # no cookies
	else:
		driver.find_element(By.XPATH, "//span[@class='cookie-button js-close-cookies']").click()

	# close login request
	try:
		element = WebDriverWait(driver, 5).until(
			EC.presence_of_element_located((By.CLASS_NAME, 'pro-promo-text'))
		)
	except TimeoutException:
		pass # no login request
	else:
		driver.find_element(By.XPATH, "//div[@class='close js-close-pro-guest']").click()


	# download file and, if necessary, rename it to avoid overwrites and ambiguous names
	driver.implicitly_wait(5)
	print("Downloading paste...")
	paste_name=url.split("/")[-1]
	## remove existing files with same name
	existing=glob.glob(downloads_path+os.sep+paste_name+".*")
	for f in existing:
		os.remove(f)

	before_download=glob.glob(downloads_path+os.sep+"*") # list of all files in downloads_path
	start=time.time()
	elapsed=0
	driver.find_element(By.LINK_TEXT, "download").click() # download
	while elapsed < 60: # set timeout
		end=time.time()
		elapsed=end-start
		after_download=glob.glob(downloads_path+os.sep+"*")
		newfile=list(set(after_download).difference(before_download)) # list of new files
		# crdownload indicates not ended download
		if len(newfile) and "crdownload" not in newfile[0]:
			break
	if len(newfile):
		if paste_name not in newfile[0]: # do nothing if the downloaded file name is correct already
			paste_ext=os.path.splitext(newfile[0])[-1]
			full_name=paste_name+paste_ext
			os.rename(newfile[0], os.path.join(downloads_path, full_name))
		else:
			full_name=newfile[0]
		print("DOWNLOAD OK")
		check_leak_update_db(os.path.join(downloads_path, full_name))
	else:
		print(f"Error downloading paste {paste_name}")

	"""
	# THIS SOL DOES NOT WORK BECAUSE pastebin.com BLOCKS THE DIRECT DOWNLOAD REQUEST
	dl_link=driver.find_element(By.LINK_TEXT, "download").get_attribute('href')
	try:
		req=driver.get(dl_link)
		with open(downloads_path+url.split('/')[-1], 'wb') as outfile:
			outfile.write(req.content)
			outfile.close()
			print("DOWNLOAD OK\n")
	except:
		raise Exception(f"Error during download of paste {url}")
	"""

	for link in hash_links[1:]: # first link is out of the loop to handle cookies and login request
		url=link['link'].replace("scrape", "www")
		print(f'Getting {url} ...')
		driver.get(url)

		# accept privacy policy
		try:
			element = WebDriverWait(driver, 10).until(
				EC.presence_of_element_located((By.CLASS_NAME, 'qc-cmp2-summary-buttons'))
		    )
		except TimeoutException:
			pass # no privacy policy
		else:
			driver.find_element(By.XPATH, "//button[@class='sc-ifAKCX ljEJIv']").click()

		# download file and, if necessary, rename it to avoid overwrites and ambiguous names
		driver.implicitly_wait(5)
		print("Downloading paste...")
		paste_name=url.split("/")[-1]
		## remove existing files with same name
		existing=glob.glob(downloads_path+os.sep+paste_name+".*")
		for f in existing:
			os.remove(f)

		before_download=glob.glob(downloads_path+os.sep+"*") # list of all files in downloads_path
		start=time.time()
		elapsed=0
		driver.find_element(By.LINK_TEXT, "download").click() # download
		while elapsed < 60: # set timeout
			end=time.time()
			elapsed=end-start
			after_download=glob.glob(downloads_path+os.sep+"*")
			newfile=list(set(after_download).difference(before_download)) # list of new files
			# crdownload indicates not ended download
			if len(newfile) and "crdownload" not in newfile[0]:
				break
		if len(newfile):
			if paste_name not in newfile[0]: # do nothing if the downloaded file name is correct already
				paste_ext=os.path.splitext(newfile[0])[-1]
				full_name=paste_name+paste_ext
				os.rename(newfile[0], os.path.join(downloads_path, full_name))
			else:
				full_name=newfile[0]
			print("DOWNLOAD OK")
		else:
			print(f"Error downloading paste {paste_name}\n")
			continue

	#	ALTERNATIVE SOL: go to pastebin.com/raw, get raw content and write it in a file
	#	url=link_el.get_attribute('text').replace("scrape.pastebin.com", "pastebin.com/raw")
	#	print(driver.find_element(By.TAG_NAME, "pre").get_attribute('innerHTML')) # raw content to write in a file


		## check presence of sha256 hashes and eventually store them in the db
		check_leak_update_db(os.path.join(downloads_path, full_name))


driver.quit()
